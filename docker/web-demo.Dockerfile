FROM continuumio/anaconda3:2019.07
MAINTAINER parkukkyu <parkukkyu@hanmail.net>

ENV DEBIAN_FRONTEND noninteractive
ENV FLASK_APP gentelella.py

ADD ./env/release.yml /web-demo/release.yml

# excute dependicies
RUN ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime && \
    buildDeps='' && \
    excuteDeps='libcurl4-openssl-dev bzip2 curl wget vim htop' && \
    apt-get update && \
    apt-get install -y $buildDeps $excuteDeps --no-install-recommends && \
    /opt/conda/bin/conda env create -f /web-demo/release.yml && \
    apt-get purge -y --auto-remove $buildDeps && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

# Copy Source
ADD ./gentelella.py /web-demo/gentelella.py
ADD ./gunicorn.py /web-demo/gunicorn.py
ADD ./config.py /web-demo/config.py
ADD ./app /web-demo/app
ADD ./batch/server.sh /web-demo/server.sh

# permission change
RUN chmod 755 /web-demo/server.sh

EXPOSE 5000

# run server
CMD [ "/web-demo/server.sh" ]
