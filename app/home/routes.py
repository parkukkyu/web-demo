import copy
import json
import mritopng
import os
import requests

from flask import render_template, request, jsonify, make_response
from flask_login import login_required, current_user
from pyecharts import Gauge
from pyecharts_javascripthon.api import TRANSLATOR

from app.home import blueprint
from app import db, options, upload_path, headers
from app.base.models import InputData, OutputData


def get_options():
    option = copy.deepcopy(options)
    option.update(InputData.query.filter_by(username=current_user.username)
                  .first().get())

    output = OutputData.query.filter_by(username=current_user.username)\
        .first().get()

    # data
    # TODO(andrew j. park) 아래 링크 확인해서 세팅하
    # https://pyecharts.org/#/zh-cn/basic_charts?id=gauge%ef%bc%9a%e4%bb%aa%e8%a1%a8%e7%9b%98
    gauge = Gauge()
    gauge.add("", "", round(output['output'], 2))

    option['chart_id'] = gauge.chart_id
    option['renderer'] = gauge.renderer
    option['my_width'] = '100%'
    option['my_height'] = 600

    javascript_snippet = TRANSLATOR.translate(gauge.options)
    option['custom_function'] = javascript_snippet.function_snippet
    option['options'] = javascript_snippet.option_snippet

    return option


@blueprint.route('/index')
@login_required
def index():
    option = get_options()
    return render_template('index.html', **option)


@blueprint.route('/<template>')
@login_required
def route_template(template):
    return render_template(template + '.html')


@blueprint.route('/upload', methods=['POST'])
@login_required
def upload():
    url = os.path.join('/static/uploads', current_user.username)
    current_user_folder = os.path.join(upload_path, current_user.username)
    mri_images = dict()
    for index, (key, file) in enumerate(request.files.items()):
        if key.startswith('file'):
            file_path = os.path.join(current_user_folder, file.filename)
            file.save(file_path)
            file_name = os.path.splitext(file.filename)[0] + '.png'
            mritopng.convert_file(file_path,
                                  os.path.join(current_user_folder, file_name),
                                  auto_contrast=True)
            mri_images['mri_image_{}'.format(index + 1)] = \
                os.path.join(url, file_name)

    db.session.query(InputData).filter_by(
        username=current_user.username).update(mri_images)
    db.session.commit()

    return make_response(jsonify('success'), 200)


@blueprint.route('/push_data', methods=['POST'])
@login_required
def push_data():
    # TODO(andrew j. park) 예외 처리 추가
    data = request.get_json()
    db.session.query(InputData).filter_by(
        username=current_user.username).update({data['name']: data['value']})
    db.session.commit()

    return make_response(jsonify('success'), 200)


@blueprint.route('/predict', methods=['POST'])
@login_required
def predict():
    input_data = db.session.query(InputData).filter_by(
        username=current_user.username).first()
    response = requests.post(options['MODEL_SERVER'] + '/predict',
                             data=json.dumps(input_data.get_request_data()),
                             headers=headers)
    # TODO(andrew j. park) 예외 처리 추가
    db.session.query(OutputData).filter_by(
        username=current_user.username)\
        .update({'output': response.json()['output']})
    db.session.commit()
    print('predict : {}'.format(response.json()['output']))
    return make_response(jsonify('success'), 200)
