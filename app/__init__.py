import ast
import os

from flask import Flask, url_for
from flask_dropzone import Dropzone
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from importlib import import_module
from logging import basicConfig, DEBUG, getLogger, StreamHandler
from os import environ
from os import path

db = SQLAlchemy()
login_manager = LoginManager()
dropzone = Dropzone()

options = dict()
options['title'] = environ.get('SITE_TITLE', 'TITLE')
options['demo_title'] = environ.get('DEMO_TITLE', 'DEMO')

options['sample_age'] = environ.get('SAMPLE_AGE', 49)
options['min_age'] = environ.get('MIN_AGE', 20)
options['max_age'] = environ.get('MAX_AGE', 85)
options['age_title'] = environ.get('AGE_TITLE', 'Age')

options['sample_duration'] = environ.get('SAMPLE_DURATION', 94)
options['min_duration'] = environ.get('MIN_DURATION', 21)
options['max_duration'] = environ.get('MAX_DURATION', 365)
options['duration_title'] = environ.get('DURATION_TITLE', 'Duration (Days)*')
DURATION_EXPLAIN = '*Duration between the completion of CCRT and the date of ' \
                   'MRI acquisition'
options['duration_explain'] = environ.get('DURATION_EXPLAIN', DURATION_EXPLAIN)


options['sample_tdose'] = environ.get('SAMPLE_TDOSE', 6000)
options['min_tdose'] = environ.get('MIN_TDOSE', 3000)
options['max_tdose'] = environ.get('MAX_TDOSE', 7000)
options['tdose_title'] = environ.get('TDOSE_TITLE', 'Total Dose (cGy)')

options['sample_tfx'] = environ.get('SAMPLE_TFX', 30)
options['min_tfx'] = environ.get('MIN_TFX', 10)
options['max_tfx'] = environ.get('MAX_TFX', 35)
options['tfx_title'] = environ.get('TFX_TITLE', 'Total Fractions (Fx)')


DEFAULT_SEX = "{\"Male\" : 1, \"Female\" : 2}"
options['sex'] = ast.literal_eval(environ.get('SEX', DEFAULT_SEX))
options['sample_sex'] = environ.get('SAMPLE_SEX', 1)
options['gender_title'] = environ.get('GENDER_TITLE', 'Gender')

DEFAULT_MGMT = "{\"Unmethylated\" : 0, \"Methylated\" : 1, \"Unknown\" : 2 }"
options['mgmt'] = ast.literal_eval(environ.get('MGMT',DEFAULT_MGMT))
options['sample_mgmt'] = environ.get('SAMPLE_MGMT', 0)
options['mgmt_title'] = environ.get('MGMT_TITLE', 'MGMT promoter status')

DEFAULT_IDH = "{\"Wild-type\" : 0, \"Mutant\" : 1, \"Unknown\" : 2}"
options['idh'] = ast.literal_eval(environ.get('IDH', DEFAULT_IDH))
options['sample_idh'] = environ.get('SAMPLE_IDH', 0)
options['idh_title'] = environ.get('IDH_TITLE', 'IDH1/2 mutation status')

options['result_title'] = environ.get('RESULT_TITLE', 'Progressive Disease')

options['input_title_size'] = environ.get('INPUT_TITLE_SIZE', '15')
options['value_name_size'] = environ.get('VALUE_NAME_SIZE', '15')

upload_path = environ.get('UPLOAD_FOLDER', os.path.join(os.path.abspath(
    os.path.dirname(__file__)), 'base/static/uploads'))

options['mri_images'] = {
    'MRI Image 1': '/static/images/01_0000.png',
    'MRI Image 2': '/static/images/01_0001.png',
    'MRI Image 3': '/static/images/01_0002.png',
    'MRI Image 4': '/static/images/01_0003.png',
    'MRI Image 5': '/static/images/01_0004.png',
    'MRI Image 6': '/static/images/01_0005.png',
    'MRI Image 7': '/static/images/01_0006.png',
    'MRI Image 8': '/static/images/01_0007.png',
    'MRI Image 9': '/static/images/01_0008.png'
}

options['HOST'] = environ.get('HOST', 'http://localhost:5000')
options['MODEL_SERVER'] = environ.get('MODEL_SERVER', 'http://localhost:8000')

headers = {'Content-Type': 'application/json; charset=utf-8'}


def register_extensions(app):
    db.init_app(app)
    login_manager.init_app(app)


def register_blueprints(app):
    for module_name in ('base', 'home', 'base'):
        module = import_module('app.{}.routes'.format(module_name))
        app.register_blueprint(module.blueprint)


def configure_database(app):

    @app.before_first_request
    def initialize_database():
        db.create_all()

    @app.teardown_request
    def shutdown_session(exception=None):
        db.session.remove()


def configure_logs(app):
    basicConfig(filename='error.log', level=DEBUG)
    logger = getLogger()
    logger.addHandler(StreamHandler())


def apply_themes(app):
    """
    Add support for themes.

    If DEFAULT_THEME is set then all calls to
      url_for('static', filename='')
      will modfify the url to include the theme name

    The theme parameter can be set directly in url_for as well:
      ex. url_for('static', filename='', theme='')

    If the file cannot be found in the /static/<theme>/ lcation then
      the url will not be modified and the file is expected to be
      in the default /static/ location
    """
    @app.context_processor
    def override_url_for():
        return dict(url_for=_generate_url_for_theme)

    def _generate_url_for_theme(endpoint, **values):
        if endpoint.endswith('static'):
            themename = values.get('theme', None) or \
                app.config.get('DEFAULT_THEME', None)
            if themename:
                theme_file = "{}/{}".format(themename, values.get('filename', ''))
                if path.isfile(path.join(app.static_folder, theme_file)):
                    values['filename'] = theme_file
        return url_for(endpoint, **values)


def create_app(config, selenium=False):
    app = Flask(__name__, static_folder='base/static')
    app.config.from_object(config)

    app.config.update(
        UPLOADED_PATH=upload_path,
        # Flask-Dropzone config:
        DROPZONE_ALLOWED_FILE_TYPE='.dcm',
        DROPZONE_MAX_FILES=9,
        DROPZONE_PARALLEL_UPLOADS=9,  # set parallel amount
        DROPZONE_UPLOAD_MULTIPLE=True,  # enable upload multiple
        DROPZONE_ALLOWED_FILE_CUSTOM=True,
        DROPZONE_TIMEOUT=300000
    )
    dropzone.init_app(app)

    if selenium:
        app.config['LOGIN_DISABLED'] = True
    register_extensions(app)
    register_blueprints(app)
    configure_database(app)
    configure_logs(app)
    apply_themes(app)
    return app
