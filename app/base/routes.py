import json
import os
import requests

from bcrypt import checkpw
from flask import jsonify, render_template, redirect, request, url_for
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user
)

from app import db, login_manager, options, upload_path, headers
from app.base import blueprint
from app.base.forms import LoginForm, CreateAccountForm
from app.base.models import User, InputData, OutputData


@blueprint.route('/')
def route_default():
    return redirect(url_for('base_blueprint.login'))


@blueprint.route('/<template>')
@login_required
def route_template(template):
    return render_template(template + '.html')


@blueprint.route('/page_<error>')
def route_errors(error):
    return render_template('errors/page_{}.html'.format(error))

## Login & Registration


@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm(request.form)
    create_account_form = CreateAccountForm(request.form)
    if 'login' in request.form:
        username = request.form['username']
        password = request.form['password']
        user = User.query.filter_by(username=username).first()
        if user and checkpw(password.encode('utf8'), user.password):
            login_user(user)
            return redirect(url_for('base_blueprint.route_default'))
        return render_template('errors/page_403.html')
    if not current_user.is_authenticated:
        return render_template(
            'login/login.html',
            login_form=login_form,
            create_account_form=create_account_form,
            **options
        )
    return redirect(url_for('home_blueprint.index'))


# TODO(andrew j. park) 예외 처리 추가
@blueprint.route('/create_user', methods=['POST'])
def create_user():
    user = User(**request.form)
    db.session.add(user)

    input_data = InputData(username=user.username,
                           gender=options['sample_sex'],
                           age=options['sample_age'],
                           mgmt=options['sample_mgmt'],
                           idh=options['sample_idh'],
                           duration=options['sample_duration'],
                           tdose=options['sample_tdose'],
                           tfx=options['sample_tfx'],
                           mri_image_1=options['mri_images']['MRI Image 1'],
                           mri_image_2=options['mri_images']['MRI Image 2'],
                           mri_image_3=options['mri_images']['MRI Image 3'],
                           mri_image_4=options['mri_images']['MRI Image 4'],
                           mri_image_5=options['mri_images']['MRI Image 5'],
                           mri_image_6=options['mri_images']['MRI Image 6'],
                           mri_image_7=options['mri_images']['MRI Image 7'],
                           mri_image_8=options['mri_images']['MRI Image 8'],
                           mri_image_9=options['mri_images']['MRI Image 9'])
    db.session.add(input_data)

    response = requests.post(options['MODEL_SERVER'] + '/predict',
                             data=json.dumps(input_data.get_request_data()),
                             headers=headers)

    # TODO(andrew j. park) 예외 처리 추가
    output_data = OutputData(username=user.username,
                             output=response.json()['output'])
    db.session.add(output_data)

    db.session.commit()

    os.mkdir(os.path.join(upload_path, user.username))
    return jsonify('success')


@blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('base_blueprint.login'))


@blueprint.route('/shutdown')
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'


## Errors


@login_manager.unauthorized_handler
def unauthorized_handler():
    return render_template('errors/page_403.html'), 403


@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('errors/page_403.html'), 403


@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('errors/page_404.html'), 404


@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('errors/page_500.html'), 500
