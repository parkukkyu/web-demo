import os

from bcrypt import gensalt, hashpw
from flask_login import UserMixin
from sqlalchemy import Binary, Column, Integer, String, Float

from app import db, login_manager, options


class User(db.Model, UserMixin):

    __tablename__ = 'User'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(Binary)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]
            if property == 'password':
                value = hashpw(value.encode('utf8'), gensalt())
            setattr(self, property, value)

    def __repr__(self):
        return str(self.username)


class InputData(db.Model):

    __tablename__ = 'InputData'

    username = Column(String, primary_key=True)
    gender = Column(Integer)
    age = Column(Integer)
    mgmt = Column(Integer)
    idh = Column(Integer)
    duration = Column(Integer)
    tdose = Column(Integer)
    tfx = Column(Integer)
    mri_image_1 = Column(String)
    mri_image_2 = Column(String)
    mri_image_3 = Column(String)
    mri_image_4 = Column(String)
    mri_image_5 = Column(String)
    mri_image_6 = Column(String)
    mri_image_7 = Column(String)
    mri_image_8 = Column(String)
    mri_image_9 = Column(String)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            setattr(self, property, value)

    def __repr__(self):
        return self.username

    def get(self):
        data = dict()
        data['sample_sex'] = self.gender
        data['sample_age'] = self.age
        data['sample_duration'] = self.duration
        data['sample_tdose'] = self.tdose
        data['sample_tfx'] = self.tfx
        data['sample_mgmt'] = self.mgmt
        data['sample_idh'] = self.idh
        data['mri_images'] = {
            'MRI Image 1': self.mri_image_1,
            'MRI Image 2': self.mri_image_2,
            'MRI Image 3': self.mri_image_3,
            'MRI Image 4': self.mri_image_4,
            'MRI Image 5': self.mri_image_5,
            'MRI Image 6': self.mri_image_6,
            'MRI Image 7': self.mri_image_7,
            'MRI Image 8': self.mri_image_8,
            'MRI Image 9': self.mri_image_9
        }
        return data

    def get_request_data(self):
        data = dict()
        data['gender'] = self.gender
        data['age'] = self.age
        data['duration'] = self.duration
        data['tdose'] = self.tdose
        data['tfx'] = self.tfx
        data['mgmt'] = self.mgmt
        data['idh'] = self.idh
        data['mri_image_1'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_1)[0] + '.dcm'
        data['mri_image_2'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_2)[0] + '.dcm'
        data['mri_image_3'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_3)[0] + '.dcm'
        data['mri_image_4'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_4)[0] + '.dcm'
        data['mri_image_5'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_5)[0] + '.dcm'
        data['mri_image_6'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_6)[0] + '.dcm'
        data['mri_image_7'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_7)[0] + '.dcm'
        data['mri_image_8'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_8)[0] + '.dcm'
        data['mri_image_9'] = options['HOST'] \
                              + os.path.splitext(self.mri_image_9)[0] + '.dcm'
        return data


class OutputData(db.Model):

    __tablename__ = 'OutputData'

    username = Column(String, primary_key=True)
    output = Column(Float)


    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            setattr(self, property, value)

    def __repr__(self):
        return self.username

    def get(self):
        data = dict()
        data['output'] = self.output
        return data


@login_manager.user_loader
def user_loader(id):
    return User.query.filter_by(id=id).first()


@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    return user if user else None
