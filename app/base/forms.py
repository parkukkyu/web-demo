from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, validators
from wtforms.fields.html5 import EmailField

## login and registration


class LoginForm(FlaskForm):
    username = TextField('Username', id='username_login')
    password = PasswordField('Password', id='pwd_login')


class CreateAccountForm(FlaskForm):
    username = TextField('Username', id='username_create')
    email = EmailField('Email', [validators.DataRequired(), validators.Email()])
    password = PasswordField('Password', id='pwd_create')
