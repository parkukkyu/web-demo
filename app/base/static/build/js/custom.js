/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */
(function($,sr){
  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
    var timeout;

    return function debounced () {
      var obj = this, args = arguments;
      function delayed () {
        if (!execAsap)
          func.apply(obj, args);
        timeout = null;
      }

      if (timeout)
        clearTimeout(timeout);
      else if (execAsap)
        func.apply(obj, args);

      timeout = setTimeout(delayed, threshold || 100);
    };
  };

  // smartresize
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
  $BODY = $('body'),
  $MENU_TOGGLE = $('#menu_toggle'),
  $SIDEBAR_MENU = $('#sidebar-menu'),
  $SIDEBAR_FOOTER = $('.sidebar-footer'),
  $LEFT_COL = $('.left_col'),
  $RIGHT_COL = $('.right_col'),
  $NAV_MENU = $('.nav_menu'),
  $FOOTER = $('footer');

	
	
// Sidebar
function init_sidebar() {
  // TODO: This is some kind of easy fix, maybe we can improve this
  var setContentHeight = function () {
    // reset height
	$RIGHT_COL.css('min-height', $(window).height());

	var bodyHeight = $BODY.outerHeight(),
      footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
      leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
      contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

      // normalize content
      contentHeight -= $NAV_MENU.height() + footerHeight;

      $RIGHT_COL.css('min-height', contentHeight);
  };

  $SIDEBAR_MENU.find('a').on('click', function(ev) {
    console.log('clicked - sidebar_menu');
    var $li = $(this).parent();

    if ($li.is('.active')) {
      $li.removeClass('active active-sm');
      $('ul:first', $li).slideUp(function() {
        setContentHeight();
      });
    } else {
      // prevent closing menu if we are on child menu
      if (!$li.parent().is('.child_menu')) {
        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
        $SIDEBAR_MENU.find('li ul').slideUp();
      } else {
        if ( $BODY.is( ".nav-sm" ) ) {
          $SIDEBAR_MENU.find( "li" ).removeClass( "active active-sm" );
          $SIDEBAR_MENU.find( "li ul" ).slideUp();
        }
      }
      $li.addClass('active');

      $('ul:first', $li).slideDown(function() {
        setContentHeight();
      });
    }
  });

  // toggle small or large menu
  $MENU_TOGGLE.on('click', function() {
    console.log('clicked - menu toggle');
		
    if ($BODY.hasClass('nav-md')) {
      $SIDEBAR_MENU.find('li.active ul').hide();
      $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
    } else {
      $SIDEBAR_MENU.find('li.active-sm ul').show();
      $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
    }

    $BODY.toggleClass('nav-md nav-sm');

    setContentHeight();

    $('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
  });

  // check active menu
  $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

  $SIDEBAR_MENU.find('a').filter(function () {
    return this.href == CURRENT_URL;
  }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
    setContentHeight();
  }).parent().addClass('active');

  // recompute content when resizing
  $(window).smartresize(function(){
    setContentHeight();
  });

  setContentHeight();

  // fixed sidebar
  if ($.fn.mCustomScrollbar) {
    $('.menu_fixed').mCustomScrollbar({
	  autoHideScrollbar: true,
      theme: 'minimal',
      mouseWheel:{ preventDefault: true }
    });
  }
};
// /Sidebar

var randNum = function() {
  return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
};


// Panel toolbox
$(document).ready(function() {
  $('.collapse-link').on('click', function() {
    var $BOX_PANEL = $(this).closest('.x_panel'),
      $ICON = $(this).find('i'),
      $BOX_CONTENT = $BOX_PANEL.find('.x_content');
        
    // fix for some div with hardcoded fix class
    if ($BOX_PANEL.attr('style')) {
      $BOX_CONTENT.slideToggle(200, function(){
        $BOX_PANEL.removeAttr('style');
      });
    } else {
      $BOX_CONTENT.slideToggle(200);
      $BOX_PANEL.css('height', 'auto');
    }

    $ICON.toggleClass('fa-chevron-up fa-chevron-down');
  });

  $('.close-link').click(function () {
    var $BOX_PANEL = $(this).closest('.x_panel');

    $BOX_PANEL.remove();
  });
});
// /Panel toolbox

// Tooltip
$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip({
    container: 'body'
  });
});
// /Tooltip


// iCheck
$(document).ready(function() {
  if ($("input.flat")[0]) {
    $(document).ready(function () {
      $('input.flat').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
      $('input.flat').on("ifClicked", function(event){
        push_data(event.target.name, parseInt(event.target.value))
      });
    });
  }
});
// /iCheck

// Table
$('table input').on('ifChecked', function () {
  checkState = '';
  $(this).parent().parent().parent().addClass('selected');
  countChecked();
});
$('table input').on('ifUnchecked', function () {
  checkState = '';
  $(this).parent().parent().parent().removeClass('selected');
  countChecked();
});

var checkState = '';

$('.bulk_action input').on('ifChecked', function () {
  checkState = '';
  $(this).parent().parent().parent().addClass('selected');
  countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
  checkState = '';
  $(this).parent().parent().parent().removeClass('selected');
  countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
  checkState = 'all';
  countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
  checkState = 'none';
  countChecked();
});

function countChecked() {
  if (checkState === 'all') {
    $(".bulk_action input[name='table_records']").iCheck('check');
  }
  if (checkState === 'none') {
    $(".bulk_action input[name='table_records']").iCheck('uncheck');
  }

  var checkCount = $(".bulk_action input[name='table_records']:checked").length;

  if (checkCount) {
    $('.column-title').hide();
    $('.bulk-actions').show();
    $('.action-cnt').html(checkCount + ' Records Selected');
  } else {
    $('.column-title').show();
    $('.bulk-actions').hide();
  }
}



// Accordion
$(document).ready(function() {
  $(".expand").on("click", function () {
    $(this).next().slideToggle(200);
    $expand = $(this).find(">:first-child");

    if ($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
});


//hover and retain popover when on popover content
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj) {
var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
  var container, timeout;

  originalLeave.call(this, obj);

  if (obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover');
    timeout = self.timeout;
    container.one('mouseenter', function() {
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function() {
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    });
  }
};

$('body').popover({
  selector: '[data-popover]',
  trigger: 'click hover',
  delay: {
    show: 50,
    hide: 400
  }
});


function gd(year, month, day) {
  return new Date(year, month - 1, day).getTime();
}


/* PARSLEY */
function init_parsley() {
  if( typeof (parsley) === 'undefined'){ return; }
  console.log('init_parsley');

  $/*.listen*/('parsley:field:validate', function() {
    validateFront();
  });
  $('#demo-form .btn').on('click', function() {
    $('#demo-form').parsley().validate();
	validateFront();
  });
  var validateFront = function() {
    if (true === $('#demo-form').parsley().isValid()) {
      $('.bs-callout-info').removeClass('hidden');
      $('.bs-callout-warning').addClass('hidden');
    } else {
      $('.bs-callout-info').addClass('hidden');
      $('.bs-callout-warning').removeClass('hidden');
    }
  };

  $/*.listen*/('parsley:field:validate', function() {
    validateFront();
  });
  $('#demo-form2 .btn').on('click', function() {
    $('#demo-form2').parsley().validate();
    validateFront();
  });
  var validateFront = function() {
    if (true === $('#demo-form2').parsley().isValid()) {
      $('.bs-callout-info').removeClass('hidden');
      $('.bs-callout-warning').addClass('hidden');
    } else {
      $('.bs-callout-info').addClass('hidden');
      $('.bs-callout-warning').removeClass('hidden');
    }
  };
			
  try {
    hljs.initHighlightingOnLoad();
  } catch (err) {}
};
	   
		
/* INPUTS */
function onAddTag(tag) {
  alert("Added a tag: " + tag);
}

function onRemoveTag(tag) {
  alert("Removed a tag: " + tag);
}

function onChangeTag(input, tag) {
  alert("Changed a tag: " + tag);
}

//tags input
function init_TagsInput() {
  if(typeof $.fn.tagsInput !== 'undefined'){
    $('#tags_1').tagsInput({
      width: 'auto'
    });
  }
};


/* KNOB */
function init_knob() {
  if( typeof ($.fn.knob) === 'undefined'){ return; }
  console.log('init_knob');
	
  $(".knob").knob({
    change: function(value) {
	  console.log("change : " + value);
    },
    release: function(value) {
      push_data(this.$.attr('id'), value)
    },
    cancel: function() {
      console.log("cancel : ", this);
    },
    draw: function() {
      // "tron" case
      if (this.$.data('skin') == 'tron') {
        this.cursorExt = 0.3;
        var a = this.arc(this.cv), // Arc
          pa, // Previous arc
          r = 1;

        this.g.lineWidth = this.lineWidth;
        if (this.o.displayPrevious) {
          pa = this.arc(this.v);
          this.g.beginPath();
          this.g.strokeStyle = this.pColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
          this.g.stroke();
        }

        this.g.beginPath();
        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
        this.g.stroke();

        this.g.lineWidth = 2;
        this.g.beginPath();
        this.g.strokeStyle = this.o.fgColor;
        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
        this.g.stroke();

		return false;
      }
    }
				  
  });

  // Example of infinite knob, iPod click wheel
  var v, up = 0,
  down = 0,
  i = 0,
  $idir = $("div.idir"),
  $ival = $("div.ival"),
  incr = function() {
    i++;
    $idir.show().html("+").fadeOut();
    $ival.html(i);
  },
  decr = function() {
    i--;
    $idir.show().html("-").fadeOut();
    $ival.html(i);
  };
  $("input.infinite").knob({
    min: 0,
    max: 20,
    stopper: false,
    change: function() {
      if (v > this.cv) {
        if (up) {
          decr();
          up = 0;
        } else {
          up = 1;
          down = 0;
        }
      } else {
        if (v < this.cv) {
          if (down) {
            incr();
            down = 0;
          } else {
            down = 1;
            up = 0;
          }
        }
      }
	  v = this.cv;
    }
  });
};


/* CUSTOM NOTIFICATION */
function init_CustomNotification() {
  console.log('run_customtabs');
			
  if( typeof (CustomTabs) === 'undefined'){ return; }
  console.log('init_CustomTabs');
			
  var cnt = 10;

  TabbedNotification = function(options) {
    var message = "<div id='ntf" + cnt + "' class='text alert-" + options.type + "' style='display:none'><h2><i class='fa fa-bell'></i> " + options.title +
        "</h2><div class='close'><a href='javascript:;' class='notification_close'><i class='fa fa-close'></i></a></div><p>" + options.text + "</p></div>";

    if (!document.getElementById('custom_notifications')) {
      alert('doesnt exists');
    } else {
      $('#custom_notifications ul.notifications').append("<li><a id='ntlink" + cnt + "' class='alert-" + options.type + "' href='#ntf" + cnt + "'><i class='fa fa-bell animated shake'></i></a></li>");
      $('#custom_notifications #notif-group').append(message);
      cnt++;
      CustomTabs(options);
    }
  };

  CustomTabs = function(options) {
    $('.tabbed_notifications > div').hide();
    $('.tabbed_notifications > div:first-of-type').show();
    $('#custom_notifications').removeClass('dsp_none');
    $('.notifications a').click(function(e) {
      e.preventDefault();
      var $this = $(this),
          tabbed_notifications = '#' + $this.parents('.notifications').data('tabbed_notifications'),
          others = $this.closest('li').siblings().children('a'),
          target = $this.attr('href');
      others.removeClass('active');
      $this.addClass('active');
      $(tabbed_notifications).children('div').hide();
      $(target).show();
    });
  };

  CustomTabs();

  var tabid = idname = '';

  $(document).on('click', '.notification_close', function(e) {
    idname = $(this).parent().parent().attr("id");
    tabid = idname.substr(-2);
    $('#ntf' + tabid).remove();
    $('#ntlink' + tabid).parent().remove();
    $('.notifications a').first().addClass('active');
    $('#notif-group div').first().css('display', 'block');
  });
};

/* COMPOSE */
function init_compose() {
  if( typeof ($.fn.slideToggle) === 'undefined'){ return; }
  console.log('init_compose');
		
  $('#compose, .compose-close').click(function(){
    $('.compose').slideToggle();
  });
};


function submit() {
  $('#input_panel').waitMe({
    //none, rotateplane, stretch, orbit, roundBounce, win8,
    //win8_linear, ios, facebook, rotation, timer, pulse,
    //progressBar, bouncePulse or img
    effect:'bounce',
    //place text under the effect (string).
    text:'',
    //background for container (string).
    bg:'rgba(255,255,255,0.7)',
    //color for background animation and text (string).
    color:'#000',
    maxSize:'',
    //wait time im ms to close
    waitTime: -1,
    //url to image
    source:'',
    //or 'horizontal'
    textPos:'vertical',
    //font size
    fontSize:'',
    // callback
    onClose:function() {}
  });

  $.ajax({
    type: 'POST',
    url: '/home/predict',
    timeout: 60000,
    success: function(result) {
      $('#input_panel').waitMe('hide');
      alertify.notify('Prediction success.', 'success', 5);
      location.reload();
    },
    error: function(result) {
      location.replace('/page_500');
    }
  });
};


function push_data(name, value) {
  $.ajax({
    type: 'POST',
    url: '/home/push_data',
    dataType: 'json',
    data: JSON.stringify({'name' : name, 'value' : value}),
    contentType: 'application/json;charset=UTF-8',
    timeout: 5000,
    success: function(result) {
      alertify.notify('Data entry success.', 'success', 5);
    },
    error: function(result) {
      location.replace('/page_500');
    }
  });
}

$(document).ready(function() {
  init_sidebar();
  init_knob();
  init_TagsInput();
  init_parsley();
  init_compose();
  init_CustomNotification();
});
