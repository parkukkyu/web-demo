/*
global
alertify: false
*/

/**
 * Create a new account.
 */
function signup() { // eslint-disable-line no-unused-vars
  if ($('#create-user-form').parsley().validate()) {
    $('#create-user-form').waitMe({
      //none, rotateplane, stretch, orbit, roundBounce, win8,
      //win8_linear, ios, facebook, rotation, timer, pulse,
      //progressBar, bouncePulse or img
      effect:'bounce',
      //place text under the effect (string).
      text:'',
      //background for container (string).
      bg:'rgba(255,255,255,0.7)',
      //color for background animation and text (string).
      color:'#000',
      maxSize:'',
      //wait time im ms to close
      waitTime: -1,
      //url to image
      source:'',
      //or 'horizontal'
      textPos:'vertical',
      //font size
      fontSize:'',
      // callback
      onClose:function() {}
    });

    $.ajax({
      type: 'POST',
      url: '/create_user',
      timeout: 60000,
      dataType: 'json',
      data: $('#create-user-form').serialize(),
      success: function(result) {
        $('#create-user-form').waitMe('hide');
        if (result == 'duplicate') {
          const message = 'Cannot create new user: duplicate entry.';
          alertify.notify(message, 'error', 5);
        } else {
          alertify.notify('New user created.', 'success', 5);
          document.getElementById('login-button').click();
        }
      },
      error: function(result) {
        location.replace('/page_500');
      }
    });
  }
}
